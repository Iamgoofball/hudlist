A set of scripts for managing list-structures for the HUD, along with an implementation for showing various information about the game state. Any information shown can be enabled/disabled at will to reduce the amount of screen space occupied, and the HUD elements can also be rescaled.

If you wish to use the core script for use in your own HUD, the HUDListCore script contains all code neccessary for adding, removing and managing the lists and items. Specific behavior of lists and items is recommended to be implemented as subclasses.

Remaining files are dedicated to the information display implementation (HUDList for short, lacking a fancier name). HUDList and HUDListClasses contain the majority of the code, with HUDListSettings containing the default display settings, and HUDListMenu containing a BLT menu implementation for changing the pre-defined settings. If the menu is used, a JSON settings file will be created and override those in HUDListSettings.

The current implementation can show a variety of information, including:

* Spawned units (enemies, civilians, hostages etc.)
* Spawned loot (separated by types or aggregated)
* Deployed equipment (bags, sentries etc.)
* Status of converted enemies
* Timers for ECM jammers, pagers, drills, hackings etc.
* Various buffs and active player effects

For a more comprehensive list, see the HUDListSettings file or the in-game menu.

IMPORTANT: To be able to use the HUDList script, it also REQUIRES installing the general-purpose information gathering script [GameInfoManager](https://bitbucket.org/pjal3urb/gameinfomanager). If this script is not loaded, the HUDList will not function, nor will it create a settings menu. Certain features are also dependent on certain GIM plugins being loaded and active; if a particular plugin is not available, the settings menu will disable the related options.
GIM was previously bundled with HUDList, but has grown beyond the original scope.
